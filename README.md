IDE - Sublime - used only as an editor with syntax highlighting etc...

Complile - SWI prolog used - Consult errington_a7.pl

Run - Two functions used - 
	--	problem_one(Asher, Ashlyn, Casey, Mckenna, Tony).
	--	problem_two(Bryce, Donald, Jane, Sawyer, Walter).

	*** These are the exact calls I used in SWI

Output - 

Welcome to SWI-Prolog (threaded, 64 bits, version 7.6.0)
SWI-Prolog comes with ABSOLUTELY NO WARRANTY. This is free software.
Please run ?- license. for legal details.

For online help and background, visit http://www.swi-prolog.org
For built-in help, use ?- help(Topic). or ?- apropos(Word).

?- problem_one(Asher, Ashlyn, Casey, Mckenna, Tony).
Asher = [4, norwegian, avila],
Ashlyn = [8, argentinean, rosa],
Casey = [9, jordanian, gillespie],
Mckenna = [10, palauan, vaughan],
Tony = [18, zimbabwean, mercado] .

?- problem_two(Bryce, Donald, Jane, Sawyer, Walter).
Bryce = [623, dancing_baby, microwave],
Donald = [220, will_it_bend, juice_press],
Jane = [515, rickrolling, blender],
Sawyer = [531, lolcat, dining_table],
Walter = [829, exploding_whale, coffee_maker] 