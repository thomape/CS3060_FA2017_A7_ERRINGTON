%% Recrusive Function calls

people([Head],Holder) :- member(Head,Holder).
people([Head|Tail],Holder) :- member(Head,Holder), people(Tail, Holder).
 
no_person([Head]) :- not(Head).
no_person([Head|Tail]) :- not(Head), no_person(Tail).
 
no_people([Head],Holder) :- not(member(Head,Holder)).
no_people([Head|Tail],Holder) :- not(member(Head,Holder)), no_people(Tail, Holder).
 
and([Head]) :- Head.
and([Head|Tail]) :- Head, and(Tail).
or([Head]) :- Head,!.
or([Head|_]) :- Head,!.
or([_|Tail]) :- or(Tail).
 
%% Problem 1 
problem_one(Asher, Ashlyn, Casey, Mckenna, Tony) :-

	%% Setting up what each person can be equal to with format
	Asher = [Asher_place, Asher_penPal, Asher_last],
	Ashlyn = [Ashlyn_place, Ashlyn_penPal, Ashlyn_last],
	Casey  = [Casey_place, Casey_penPal, Casey_last],
	Mckenna  = [Mckenna_place, Mckenna_penPal, Mckenna_last],
	Tony = [Tony_place, Tony_penpal, Tony_last],
	All = [Asher, Ashlyn, Casey, Mckenna, Tony],

	%% Facts that cover every possble outcome - meaning each person has the opprotunity to be each one of these
	people([4,8,9,10,18], [Asher_place, Ashlyn_place, Casey_place, Mckenna_place, Tony_place]),
	people([argentinean, jordanian, norwegian, palauan, zimbabwean], [Asher_penPal, Ashlyn_penPal, Casey_penPal, Mckenna_penPal, Tony_penpal]),
	people([avila, gillespie, mercado, rosa, vaughan], [Asher_last, Ashlyn_last, Casey_last, Mckenna_last, Tony_last]),

	%% Rules
	%% 1
	member([4,_,avila], All),

	%% 2
	member([_,norwegian,C2_name], All),
	not(C2_name = norwegian),

	%% 3
	member([_,_,mercado], All),
	member([_,palauan,_], All),
	member([18,_,_], All),
	member([_,_,avila], All),
	no_people([Asher_last],[mercado]),
	no_people([Asher_place],[18]),
	no_people([Asher_penPal],[palauan]),
	no_people([Asher],[avila]),

	%% 4
	member([_,argentinean,C4_name], All),
	not(C4_name = argentinean),

	%% 5 
	member([C5_place,_,mercado], All),
	member([C5_place2, argentinean, _], All),
	C5_place > C5_place2,

	%% 6
	member([Mckenna_place,Mckenna_penPal,Mckenna_last], All),
	member([C6_place,_,mercado], All),
	Mckenna_place < C6_place,

	%% 7
	member([_,argentinean,C9_last], All),
	not(C9_last = avila),
	not(C9_last = gillespie),

	%% 8
	member([9,_,C8_name], All),
	not(C8_name = 9),

	%% 9
	member([_,jordanian,Casey_last], All),

	%% 10
	member([Ashlyn_place,Ashlyn_penPal,Ashlyn_last], All),
	member([C10_place,palauan,_], All),
	Ashlyn_place < C10_place,

	%% 11
	member([Tony_place,_,Tony_last], All),
	member([Mckenna_place,_,Mckenna_last], All).


%% Problem 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
problem_two(Bryce, Donald, Jane, Sawyer, Walter) :-

	%% Setting up what each person can be equal to with format
	Bryce = [Bryce_date, Bryce_craze, Bryce_gift],
	Donald = [Donald_date, Donald_craze, Donald_gift],
	Jane = [Jane_date, Jane_craze, Jane_gift],
	Sawyer = [Sawyer_date, Sawyer_craze, Sawyer_gift],
	Walter = [Walter_date, Walter_craze, Walter_gift],
	All = [Bryce, Donald, Jane, Sawyer, Walter],

	%% Facts that cover every possble outcome - meaning each person has the opprotunity to be each one of these
	people([220, 515, 531, 623, 829], [Bryce_date, Donald_date, Jane_date, Sawyer_date, Walter_date]),
	people([dancing_baby, exploding_whale, lolcat, rickrolling, will_it_bend], [Bryce_craze, Donald_craze, Jane_craze, Sawyer_craze, Walter_craze]),
	people([blender, coffee_maker, dining_table, juice_press, microwave], [Bryce_gift, Donald_gift, Jane_gift, Sawyer_gift, Walter_gift]),


	%% Rules
	%% 1
	member([Bryce_date,_,_], All),
	member([Sawyer_date,_,_], All),
	Bryce_date > Sawyer_date,

	%% 2
	member([_,rickrolling,_], All),
	member([531,_,_], All),
	member([_,_,dining_table], All),
	member([_,lolcat,_], All),
	no_people([Walter_date],[531]),
	no_people([Walter_gift],[dining_table]),
	no_people([Walter_craze],[lolcat, rickrolling]),

	%% 3
	member([Bryce_date, Bryce_craze, microwave], All),

	%% 4
	member([Jane_date,_,_], All),
	member([C4_date,_,juice_press], All),
	Jane_date > C4_date,

	%% 5
	member([Walter_date,_,Walter_gift], All),
	member([C5_date,dancing_baby,C5_gift], All),
	or([and([Walter_date = 829, C5_gift = microwave]), and([Walter_gift = microwave, C5_date = 829])]),

	%% 6
	member([Walter_date, exploding_whale, Walter_gift], All),

	%% 7
	member([C7_1,_,juice_press], All),
	member([C7_2,_,coffee_maker], All),
	C7_1 < C7_2,

	%% 8 
	member([C8_date,will_it_bend,_], All),
	or([C8_date = 220,C8_date = 829]),

	%% 9
	member([_,lolcat, C9_gift], All),
	not(C9_gift = juice_press),

	%% 10
	member([_,_,Jane_gift], All),
	not(Jane_gift = coffee_maker),

	%% 11
	member([_,Sawyer_craze ,_], All),
	not(Sawyer_craze = rickrolling).